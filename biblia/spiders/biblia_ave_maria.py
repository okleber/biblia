# -*- coding: utf-8 -*-
from scrapy import Spider
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

class BibliaAveMariaSpider(Spider):
    name = 'biblia-ave-maria'
    allowed_domains = ['www.bibliacatolica.com.br']
    start_urls = ['http://www.bibliacatolica.com.br']

    rules = [
        Rule(LinkExtractor(allow=['.*']))
    ]
    def parse(self, response):
        pass
